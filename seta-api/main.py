from elasticsearch import Elasticsearch
from math import sqrt
from flask import Flask, jsonify, request, url_for, redirect, send_from_directory, g, Response
# This is a required workaround for flask_restplus
import werkzeug
from werkzeug.utils import secure_filename
werkzeug.cached_property = werkzeug.utils.cached_property
import flask.scaffold
flask.helpers._endpoint_from_view_func = flask.scaffold._endpoint_from_view_func
# Workaround end
from flask_restplus import Resource, Api, reqparse, inputs, fields
import pandas as pd
import json
import copy
import yaml
import datetime
from gensim.models import KeyedVectors
from tqdm import tqdm
from sklearn.cluster import DBSCAN
from flask_cors import CORS
import re
from gensim.models.doc2vec import Doc2Vec
import itertools
import nltk
from nltk.tokenize import word_tokenize
from nltk.text import ConcordanceIndex
from flask_jwt_extended import JWTManager, create_access_token, decode_token, get_jwt, verify_jwt_in_request
from functools import wraps
import os
import os.path
import subprocess
from pymongo import MongoClient

from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
from Crypto.Signature import pkcs1_15
import binascii

from log_utils.api_log import ApiLog
from log_utils.log_line import LogLine
import time
from sentence_transformers import SentenceTransformer
from utils.jrcbox_download import seta_init


config = yaml.load(open("config.yaml"), Loader=yaml.FullLoader)


def now():
    return datetime.datetime.now()


def init():
    seta_init(config)
#    if config['env'] == 'dev':
#        models_path = config['models-path-dev']
#        es = Elasticsearch(config["es-host-dev"], http_auth=(config["user"], config["pass"]))
#    else:
#        models_path = config['models-path-production']
#        es = Elasticsearch(config["es-host"], http_auth=(config["user"], config["pass"]))
#    total = es.count(index=config["index"])['count']
#    print(now(), "Total number of documents indexed by Elastic:", total)
    # models new folder
    # word2vec = KeyedVectors.load(models_path + "models-production/wv-sg0-hs1.bin", mmap="r")
    # models June 2021 folder
#    word2vec = KeyedVectors.load(models_path + "wv-sg0hs1.bin", mmap="r")
#    print(now(), "Loaded %s" % word2vec)

    es = Elasticsearch(config["es-host"])
    total = es.count(index=config["index"])['count']
    models_path = config['models-path']
    print(now(), "Total number of documents indexed by Elastic:", total)
#    if not os.path.isfile(config['models-word2vec-file']) :
       
    word2vec = KeyedVectors.load(models_path + config['models-word2vec-file'], mmap="r")
    print(now(), "Loaded %s" % word2vec)

    lookup = {}
#    for word in tqdm(list(word2vec.wv.vocab)):
#        for wn in range(len(word)):
#            w = word[:wn + 1]
#            if w not in lookup: lookup[w] = []
#            if len(lookup[w]) < 10: lookup[w].append(word)
#    print('Lookup field created')
    sbert = SentenceTransformer('all-distilroberta-v1')
    sbert.max_seq_length = 512
    print("SeTA-API is up and running.", flush=True)
    return es, word2vec, lookup, sbert

es, terms_model, lookup, sbert_model = init()
app = Flask(__name__)
# app.wsgi_app = ProxyFix(app.wsgi_app, x_proto=1, x_host=1)
CORS(app)
app.config["JWT_SECRET_KEY"] = config['secret-key']
app.config["PROPAGATE_EXCEPTIONS"] = True
app.config["SWAGGER_UI_JSONEDITOR"] = True
jwt = JWTManager(app)

api_log = ApiLog()

authorizations = {
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization',
        'description': "Type in the *'Value'* input box below: **'Bearer &lt;JWT&gt;'**, where JWT is the token"
    }
}

app.config.SWAGGER_UI_DOC_EXPANSION = 'full'
ns = Api(app,
         version='beta',
         title='SeTA API',
         description='SeTA<style>.models {display: none !important}</style> - Semantic Text Analysis. \n'
                     'SeTa applies advanced text analysis techniques to large document collections, helping policy '
                     'analysts to understand the concepts expressed in thousands of documents and to see in a visual '
                     'manner the relationships between these concepts and their development over time.'
                     'A pilot version of this tool has been populated with hundreds of thousands of documents from '
                     'EUR-Lex, the EU Bookshop and other sources, and used at the JRC in a number of policy-related '
                     'use cases including impact assessment, the analysis of large data infrastructures, '
                     'agri-environment measures and natural disasters. The document collection which have been used, '
                     'the technical approach chosen and key use cases are described here: '
                     'https://ec.europa.eu/jrc/en/publication/semantic-text-analysis-tool-seta',
         doc='/seta-api/doc',
         authorizations=authorizations
         )
ns = ns.namespace('seta-api', description='Seta APIs include different operation such as browsing documents, '
                                          'clustering term, creating ontology and other tasks '
                                          'like extract semantic distance between two terms.')

DEFAULT_SUGGESTION = 6
DEFAULT_TERM_NUMBER = 20
DEFAULT_DOCS_NUMBER = 10
DEFAULT_FROM_DOC_NUMBER = 0
api_root = '/api/v1'
CONNECTION_STRING = config["mongodb-host"] # 'mongodb://127.0.0.1:27017/'
client = MongoClient(CONNECTION_STRING)
mongodb_seta = client.seta


def custom_validator():
    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            verify_jwt_in_request()
            claims = get_jwt()
            print(claims)
            token_str = request.headers['Authorization']
            revoked_token = mongodb_seta.users.find_one({"username": claims['sub'],
                                       "is-revoked-token": True,
                                       "jwt": token_str})
            print('revoked', revoked_token)
            if not revoked_token:
                return fn(*args, **kwargs)
            else:
                response = jsonify({"msg": "Roveked token"})
                response.status_code = 403
                return response
        return decorator
    return wrapper


def autenticate_user(username, message, signature):
    item = mongodb_seta.users.find({"username": username, "is-rsa-key": True, "is-public-key": True, "is-private-key": False})
    if item.count() > 0:
        public = item[0]['value']
    else:
        response = jsonify({"error": "Invalid Username"})
        response.status_code = 501
        return False, response
    public_key = RSA.import_key(public)
    digest = SHA256.new(message.encode())
    try:
        pkcs1_15.new(public_key).verify(digest, binascii.unhexlify(signature))
    except Exception as e:
        response = jsonify({"error": str(e)})
        response.status_code = 502
        return False, response
    return True, None


@ns.route('/example_get-token_guest.py')
class ExampleGuest(Resource):
    def get(self):
        return send_from_directory(directory='static', path=config['example-guest'])


@ns.route('/example_get-token_user.py')
class ExampleUser(Resource):
    def get(self):
        return send_from_directory(directory='static', path=config['example-user'])


auth_data = ns.model(
    "get_token_params",
    {'username': fields.String(description="Username"),
     'rsa_original_message': fields.String(description="Original message"),
     'rsa_message_signature': fields.String(description="Signature using hex format, string of hexadecimal numbers.")})


@app.before_request
def before_request():
    g.start = time.time()


@app.after_request
def after_request(response: Response):
    diff = time.time() - g.start
    header = request.headers
    username = None
    if "Authorization" in header:
        token = str.replace(request.headers['Authorization'], 'Bearer ','')
        try:
            username = decode_token(token)['sub']
        except:
            pass

    error_message = None

    if response.status_code != 200 and response.json:
        if 'error' in response.json:
            error_message = response.json["error"]
        if 'msg' in response.json:
            error_message = response.json["msg"]

    line = LogLine(username, str(now()), request.remote_addr, request.full_path, response.status_code,
                   diff, error_message)
    api_log.write_log(line)
    return response


@ns.route(api_root + "/get-token", methods=['POST', 'GET'])
class JWTtoken(Resource):
    @ns.doc(description="JWT token for users, expiration 1 day.\n"
                        'Python example <a href="example_get-token_user.py" target="_blank" download="example_get-token_user.py">here</a>',
            responses={200: 'Success',
                       501: 'Invalid Username',
                       502: 'Invalid Signature'})
    @ns.expect(auth_data)

    def post(self):
        args = request.get_json(force=True)
        valid, response = autenticate_user(args['username'], args['rsa_original_message'],
                                           args['rsa_message_signature'])
        if valid:
            access_token = "Bearer " + create_access_token(identity=args['username'],
                                                           expires_delta=datetime.timedelta(days=1))
            return jsonify(access_token=access_token)
        else:
            return response

    @ns.doc(description='JWT token for GUEST users, expiration 1 hour.\n'
                        'Python example <a href="example_get-token_guest.py" target="_blank" download="example_get-token_guest.py">here</a>',
            responses={200: 'Success'})
    def get(self):
        user_number = str(time.time())
        access_token = "Bearer " + create_access_token(identity="guest-" + user_number, expires_delta=datetime.timedelta(hours=1))
        return jsonify(access_token=access_token)


cluster_parser = reqparse.RequestParser()
cluster_parser.add_argument('term', required=True)
cluster_parser.add_argument('n_terms', type=int)


@ns.route(api_root + "/clusters")
@ns.doc(description='Given a term, the 20 most similar terms are extracted. '
                    'Terms are then clustered using the algorithm DBSCAN. '
                    'Clusters may not exist and some terms can be unclustered.'
                    'The number of terms to be clustered can be customized with the parameter n_terms',
        params={'term': 'Term to be searched.',
                'n_terms': 'The number of terms to be clustered (default 20, max 30).'},
        responses={200: 'Success', 404: 'Not Found Error'},
        security='apikey')
class Clusters(Resource):
    @custom_validator()
    @ns.expect(cluster_parser)
    def get(self):
        args = cluster_parser.parse_args()
        response = get_clusters(args['term'], args['n_terms'])
        return response


suggestions_parser = reqparse.RequestParser()
suggestions_parser.add_argument('chars', required=True)
suggestions_parser.add_argument('n_suggestions', type=int)


@ns.route(api_root + "/suggestions")
@ns.doc(description="Retrieve terms by initial letters. By default it returns 6 terms,"
                    " with the parameter n_suggestions is possible to set the number of suggestions to be shown.",
        params={'chars': 'Initial letters.', 'n_suggestions': 'Number of terms to be returned (default 6).'},
        security='apikey')
@ns.expect(suggestions_parser)
class Suggestions(Resource):
    @custom_validator()
    def get(self):
        args = suggestions_parser.parse_args()
        return get_word_suggestions(args['chars'], args['n_suggestions'])


@ns.route(api_root + "/corpus/<string:id>")
@ns.doc(description='Given an ID, the relative document from EU corpus is shown.'
                    'EU corpus contains documents of the European Commission: '
                    'Eur-Lex, CORDIS,JRC PUBSY, EU Open Data Portal, etc..',
        params={'id': 'Return the document with the specified ID'},
        responses={200: 'Success', 404: 'Not Found Error'},
        security='apikey')
class Corpus(Resource):
    @custom_validator()
    def get(self, id):
        return docbyid(id)


corpus_parser = reqparse.RequestParser()
corpus_parser.add_argument('term')
corpus_parser.add_argument('n_docs', type=int)
corpus_parser.add_argument('from_doc', type=int)
corpus_parser.add_argument('source', action='split')
corpus_parser.add_argument('sector', action='split')
corpus_parser.add_argument('subject', action='split')
corpus_parser.add_argument('res_type', action='split')
corpus_parser.add_argument('eurovoc_dom', action='split')
corpus_parser.add_argument('eurovoc_mth', action='split')
corpus_parser.add_argument('eurovoc_tt', action='split')
corpus_parser.add_argument('eurovoc_concept', action='split')
corpus_parser.add_argument('conc_dir_1', action='split')
corpus_parser.add_argument('conc_dir_2', action='split')
corpus_parser.add_argument('conc_dir_3', action='split')
corpus_parser.add_argument('info_force', type=inputs.boolean)
corpus_parser.add_argument('sort', action='split')
corpus_parser.add_argument('semantic_sort_id')
corpus_parser.add_argument('author', action='split')
corpus_parser.add_argument('date_range', action='split')
corpus_parser.add_argument('aggs')

insert_user_data = ns.model(
    "corpus_params",
    {
        'term': fields.String(description="term to be searched", example="data"),
        'n_docs': fields.Integer(description='Number of documents to be shown (default 10)', example=10),
        'from_doc': fields.Integer(description='Defines the number of hits to skip, defaulting to 0.', example=0),
        'source': fields.List(fields.String, description='By default contains all the corpus: '
                                                         'eurlex,bookshop,cordis,pubsy,opendataportal.'
                                                         'It is possible to choose from which corpus retrieve documents.'),
        'sector': fields.List(fields.String, description='eurlex metadata id_sector'),
        'subject': fields.List(fields.String, description='eurlex metadata subject_matter_1'),
        'res_type': fields.List(fields.String, description='eurlex metadata list_ressource_type'),
        'eurovoc_dom': fields.List(fields.String, description='eurlex metadata eurovoc_dom'),
        'eurovoc_mth': fields.List(fields.String, description='eurlex metadata eurovoc_mth'),
        'eurovoc_tt': fields.List(fields.String, description='eurlex metadata eurovoc_tt'),
        'eurovoc_concept': fields.List(fields.String, description='eurlex metadata eurovoc_concept'),
        'conc_dir_1': fields.List(fields.String, description='eurlex metadata concept_directory_1'),
        'conc_dir_2': fields.List(fields.String, description='eurlex metadata concept_directory_2'),
        'conc_dir_3': fields.List(fields.String, description='eurlex metadata concept_directory_3'),
        'info_force': fields.Boolean(description='eurlex metadata info_force'),
        'sort': fields.List(fields.String, description='sort results field:order'),
        'semantic_sort_id': fields.String(description='sort results by semantic distance among documents'),
        'emb_vector': fields.List(fields.Float, description='embeddings vector'),
        'author': fields.String(description='author'),
        'date_range': fields.List(fields.String,
                                  description='examples: gte:yyyy-mm-dd,lte:yyyy-mm-dd,gt:yyyy-mm-dd,lt:yyyy-mm-dd'),
        'aggs': fields.String(description='field to be aggregated, allowed fields are: "date_year", "source", "doc_type", "eurovoc_concept"')
    }
)


@ns.route(api_root + "/corpus", methods=['POST', 'GET'])
class CorpusQuery(Resource):
    @custom_validator()
    @ns.doc(description='Retrieve documents related to a term from EU corpus.'
                        'EU corpus contains documents of the European Commission: '
                        'Eur-Lex, CORDIS, JRC PUBSY, EU Open Data Portal, etc..',
            security='apikey')
    @ns.expect(insert_user_data)
    def post(self):
        args = request.get_json(force=True)
        if is_field_in_doc(args, 'term') or is_field_in_doc(args, 'semantic_sort_id') \
                or is_field_in_doc(args, 'emb_vector') or is_field_in_doc(args, 'aggs'):
            return corpus(is_field_in_doc(args, 'term'), is_field_in_doc(args, 'n_docs'),
                          is_field_in_doc(args, 'from_doc'),
                          is_field_in_doc(args, 'source'), is_field_in_doc(args, 'sector'),
                          is_field_in_doc(args, 'subject'),
                          is_field_in_doc(args, 'res_type'), is_field_in_doc(args, 'eurovoc_dom'),
                          is_field_in_doc(args, 'eurovoc_mth'),
                          is_field_in_doc(args, 'eurovoc_tt'), is_field_in_doc(args, 'eurovoc_concept'),
                          is_field_in_doc(args, 'conc_dir_1'), is_field_in_doc(args, 'conc_dir_2'),
                          is_field_in_doc(args, 'conc_dir_3'), is_field_in_doc(args, 'info_force'),
                          is_field_in_doc(args, 'sort'),
                          is_field_in_doc(args, 'semantic_sort_id'), is_field_in_doc(args, 'emb_vector'),
                          is_field_in_doc(args, 'author'), is_field_in_doc(args, 'date_range'),
                          is_field_in_doc(args, 'aggs'))

    @ns.expect(corpus_parser)
    @custom_validator()
    @ns.doc(description='Retrieve documents related to a term from EU corpus.'
                        'EU corpus contains documents of the European Commission: '
                        'Eur-Lex, CORDIS, JRC PUBSY, EU Open Data Portal, etc..',
            params={'term': 'Return documents related to the specified term',
                    'n_docs': 'Number of documents to be shown (default 10).',
                    'from_doc': 'Defines the number of hits to skip, defaulting to 0.',
                    'source': 'By default contains all the corpus: '
                              'eurlex,bookshop,cordis,pubsy,opendataportal.'
                              'It is possible to choose from which corpus retrieve documents.',
                    'sector': 'eurlex metadata id_sector',
                    'subject': 'eurlex metadata subject_matter_1',
                    'res_type': 'eurlex metadata list_ressource_type',
                    'eurovoc_dom': 'eurlex metadata eurovoc_dom',
                    'eurovoc_mth': 'eurlex metadata eurovoc_mth',
                    'eurovoc_tt': 'eurlex metadata eurovoc_tt',
                    'eurovoc_concept': 'eurlex metadata eurovoc_concept',
                    'conc_dir_1': 'eurlex metadata concept_directory_1',
                    'conc_dir_2': 'eurlex metadata concept_directory_2',
                    'conc_dir_3': 'eurlex metadata concept_directory_3',
                    'info_force': 'eurlex metadata info_force',
                    'sort': 'sort results field:order',
                    'semantic_sort_id': 'sort results by semantic distance among documents',
                    'author': 'description',
                    'date_range': 'gte:yyyy-mm-dd,lte:yyyy-mm-dd,gt:yyyy-mm-dd,lt:yyyy-mm-dd',
                    'aggs': 'field to be aggregated, allowed fields are: "date_year", "source", "doc_type", "eurovoc_concept"'},
            security='apikey')
    def get(self):
        args = corpus_parser.parse_args()
        if args['term'] or args['semantic_sort_id'] or args['aggs']:
            return corpus(args['term'], args['n_docs'], args['from_doc'], args['source'], args['sector'],
                          args['subject'], args['res_type'], args['eurovoc_dom'], args['eurovoc_mth'],
                          args['eurovoc_tt'], args['eurovoc_concept'], args['conc_dir_1'], args['conc_dir_2'],
                          args['conc_dir_3'], args['info_force'], args['sort'], args['semantic_sort_id'], None,
                          args['author'], args['date_range'], args['aggs'])


@ns.doc(description='Given an ID, the relative document from Wikipedia is shown.',
        params={'id': 'Return the document with the specified ID'},
        responses={200: 'Success', 404: 'Not Found Error'},
        security='apikey')
@ns.route(api_root + "/wiki/<string:id>")
class Wiki(Resource):
    @custom_validator()
    def get(self, id):
        return wikibyid(id)


document_parser = reqparse.RequestParser()
document_parser.add_argument('term')
document_parser.add_argument('n_docs', type=int)


@ns.doc(description='Retrieve documents related to a term from Wikipedia',
        params={'term': 'Return documents related to the specified term.',
                'n_docs': 'Number of documents to be shown (default 10).'},
        security='apikey')
@ns.route(api_root + "/wiki")
class WikiQuery(Resource):
    @custom_validator()
    @ns.expect(document_parser)
    def get(self):
        args = document_parser.parse_args()
        if args['term']:
            return wikisearch(args['term'], args['n_docs'])


similar_parser = reqparse.RequestParser()
similar_parser.add_argument('term', required=True)
similar_parser.add_argument('n_term', type=int)


@ns.doc(description='Given a term, return the 20 most similar terms (semantic similarity). '
                    'For each term similarity and cardinality '
                    '(number of occurrences in documents) are reported.',
        params={'term': 'The original term.',
                'n_term': 'Number of similar terms to be extracted (default 20).'},
        responses={200: 'Success', 404: 'Not Found Error'},
        security='apikey')
@ns.route(api_root + "/similar")
class SimilarWords(Resource):
    @custom_validator()
    @ns.expect(similar_parser)
    def get(self):
        args = similar_parser.parse_args()
        return get_similar_words(args['term'], args['n_term'])


term_parser = reqparse.RequestParser()
term_parser.add_argument('term', required=True)


@ns.route(api_root + "/ontology")
@ns.doc(description='Return a graph that describes the ontology of the specified term. '
                    'A set of nodes and relative links are provided.'
                    'For each node depth, id, size and graph size are returned, '
                    'depth indicates the depth of the node in the graph, id is the identifier of the term for the node,'
                    ' size indicates the number of occurrences of the term in the document corpus '
                    'and graph size is useful to visualize the graph.'
                    'For each link source, target and value are returned, '
                    'source indicates the node (its id) from which the link starts, '
                    'target is the node (its id) linked to source'
                    'and value is used to visualize the graph.',
        params={'term': 'The term from which build the ontology graph.'},
        responses={200: 'Success', 404: 'Not Found Error'},
        security='apikey')
class Ontology(Resource):
    @custom_validator()
    @ns.expect(term_parser)
    def get(self):
        args = term_parser.parse_args()
        return build_graph(args['term'])


@ns.route(api_root + "/ontology-list")
@ns.doc(description='Return a list of lists of similar terms that describes the ontology of the specified term. '
                    'Lists are ranked by the relation strenght to a query term. The first node in each list is'
                    ' direct relation to query term. The following terms in each sublist have relation to'
                    ' the first node in a sublist.'
                    'The result should be interpretd as follows: the first item in each sublist is first level'
                    ' connection to the query term. The following terms in sublists have second level relation'
                    ' to the main query term and direct connection to the head of sublist.',
        params={'term': 'The term from which build the ontology tree.'},
        responses={200: 'Success', 404: 'Not Found Error'},
        security='apikey')
class OntologyList(Resource):
    @custom_validator()
    @ns.expect(term_parser)
    def get(self):
        args = term_parser.parse_args()
        return build_tree(args['term'])


@ns.route(api_root + "/decade")
@ns.doc(description='Return data that describes how documents are placed among decades.',
        params={'term': 'The term'},
        security='apikey')
class DecadeGraph(Resource):
    @custom_validator()
    @ns.expect(term_parser)
    def get(self):
        args = term_parser.parse_args()
        return build_decade_graph(args['term'])


@ns.route(api_root + "/term-exists")
@ns.doc(description='Return True if the word exists in the trained model else False',
        params={'term': 'The term'},
        security='apikey')
class Term(Resource):
    @custom_validator()
    @ns.expect(term_parser)
    def get(self):
        args = term_parser.parse_args()
        return word_exists(args['term'])


distance_parser = reqparse.RequestParser()
distance_parser.add_argument('term1', required=True)
distance_parser.add_argument('term2', required=True)


@ns.route(api_root + "/distance")
@ns.doc(description='Return the semantic distance (cosine distance of vectors) between two terms.',
        params={'term1': 'First term', 'term2': 'Second term'},
        responses={200: 'Success', 404: 'Not Found Error'},
        security='apikey')
class Distance(Resource):
    @custom_validator()
    @ns.expect(distance_parser)
    def get(self):
        args = distance_parser.parse_args()
        return semantic_distance(args['term1'], args['term2'])


most_similar_parser = reqparse.RequestParser()
most_similar_parser.add_argument('term', required=True)
most_similar_parser.add_argument('term_list', required=True, action='split')
most_similar_parser.add_argument('top_n', type=int)


@ns.route(api_root + "/most-similar")
@ns.doc(description='Given a term and a list of terms (comma separated list), '
                    'return the 3 terms in the list that are more similar with the initial term.'
                    'Similarities are computed using semantic distance.',
        params={'term': 'The initial term.',
                'term_list': 'The list of terms from whom extract the most similar ones to the initial '
                             '(comma separated list).',
                'top_n': 'The number of terms that are returned, default 3, maximum the length of the list.'},
        responses={200: 'Success', 404: 'Not Found Error'},
        security='apikey')
class MostSimilar(Resource):
    @custom_validator()
    @ns.expect(most_similar_parser)
    def get(self):
        args = most_similar_parser.parse_args()
        return most_similar(args['term'], args['term_list'], args['top_n'])


parser_file = reqparse.RequestParser()
parser_file.add_argument('file', type=werkzeug.datastructures.FileStorage, location='files')
parser_file.add_argument('text')


def compute_embeddings(text):
    if text is None:
        response = jsonify('No text provided.')
        response.status_code = 404
        return response
    vector = sbert_model.encode([text], convert_to_numpy=True)
    emb = {
        "embeddings": {
            "version": "sbert_vector - SentenceTransformer model all-distilroberta-v1",
            "vector": vector[0].tolist()
        }
    }
    return jsonify(emb)


def process_file(file):
    dir_name = os.path.dirname(__file__)
    target = os.path.join(dir_name, '.')
    filename = secure_filename(file.filename)
    destination = os.path.join(target, filename)
    file.save(destination)
    parsed_file = subprocess.check_output(['java', '-jar', 'tika/tika-app-2.1.0.jar', '--text', destination])
    parsed_content = parsed_file.decode('utf-8')
    os.remove(destination)
    return parsed_content


@ns.route(api_root + "/compute_embeddings")
@ns.doc(
    description='Given a file or a plain text, related embeddings are provided. Embeddings are built using Doc2vec. '
                'Tika is used to extract text from the provided file. '
                'If both file and text are provided, function will return text embeddings.',
    params={'file': 'File to be uploaded from which building embeddings.',
            'text': 'Plain text from which building embeddings.'},
    responses={200: 'Success'},
    security='apikey')
class ComputeEmb(Resource):
    @custom_validator()
    @ns.expect(parser_file)
    def post(self):
        args = parser_file.parse_args()
        print(args)
        if args['text']:
            return compute_embeddings(args['text'])
        if args['file']:
            return compute_embeddings(process_file(args['file']))
        else:
            response = jsonify('No text provided.')
            response.status_code = 400
            return response


def get_word_suggestions(chars, n_suggestions=6):
    if n_suggestions is None:
        n_suggestions = DEFAULT_SUGGESTION
    path = sanitize_input(chars)
    return jsonify({"words": lookup[path][:n_suggestions] if path in lookup else []})


def sanitize_input(word):
    return word.replace(' ', '_').lower()


def get_similar_words(term, n_term):
    if n_term is None:
        n_term = DEFAULT_TERM_NUMBER
    term = sanitize_input(term)
    words = {'words': []}
    if not word_exists(term):
        response = jsonify(words)
        response.status_code = 200
        return response
    for x, y in terms_model.wv.most_similar(term, topn=n_term):
        words['words'].append({"similarity": str(y)[:4],
                               "similar_word": x.replace('_', ' '),
                               "cardinality": str(+terms_model.wv.vocab[x].count)})
    return jsonify(words)


def extract_doctype(doctype_list):
    doctype = []
    for element in doctype_list:
        if len(element) <= 2:
            doctype.append(element)
    return doctype


def is_field_in_doc(source, field):
    if field in source:
        return source[field]
    else:
        return None


def celex_links_to_json(document):
    if 'CELEX:' in document['longid']:
        json_links = []
        if 'links' in document:
            if len(document['links']) == 1:
                json_links.append(
                    {"link": "https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=" + document['longid'],
                     "type": "pdf"})
            if len(document['links']) > 1:
                json_links.append(
                    {"link": "https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=" + document['longid'],
                     "type": "html"})
                json_links.append(
                    {"link": "https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=" + document['longid'],
                     "type": "pdf"})

        json_links.append(
            {"link": "https://eur-lex.europa.eu/legal-content/EN/ALL/?uri=" + document['longid'],
             "type": "all"})
        return json_links
    else:
        return None


def get_vector(semantic_sort_id):
    query = {"query": {"bool": {"must": [{"match": {"longid.keyword": {"query": semantic_sort_id}}}]}},
             "_source": ["sbert_vector"]}
    res = es.search(index=config['index'], body=query)
    vector = None
    if res['hits']['total']['value'] > 0:
        vector = res['hits']['hits'][0]['_source']['sbert_vector']
    return vector


def corpus(term, n_docs, from_doc, sources, sector, subject, res_type, eurovoc_dom, eurovoc_mth, eurovoc_tt,
           eurovoc_concept, conc_dir_1, conc_dir_2, conc_dir_3, info_force, sort, semantic_sort_id, emb_vector, author,
           date_range, aggs):
    documents = {"total_docs": None, "documents": []}
    list_of_aggs_fields = ["date_year", "source", "doc_type", "eurovoc_concept"]
    if aggs and aggs not in list_of_aggs_fields:
        response = jsonify('Malformed query. Wrong aggs parameter')
        response.status_code = 404
        return response
    body = build_corpus_request(term, n_docs, from_doc, sources, sector, subject, res_type, eurovoc_dom, eurovoc_mth,
                                eurovoc_tt, eurovoc_concept, conc_dir_1, conc_dir_2, conc_dir_3, info_force, sort,
                                semantic_sort_id, emb_vector, author, date_range, aggs)
    print(body)
    res = es.msearch(body=body)
    for k in res["responses"]:
        if "error" in k:
            response = jsonify('Malformed query.')
            response.status_code = 404
            return response
        documents["total_docs"] = k['hits']['total']['value']
        if semantic_sort_id is not None and term is None and documents["total_docs"] > 100:
            documents["total_docs"] = 100
        tot = 0
        if aggs:
            documents["aggregations"] = k["aggregations"]["types"]["buckets"]
        for document in k["hits"]["hits"]:
            if tot >= documents["total_docs"]:
                break
            title = document['_source']['title'] if isinstance(document['_source']['title'], str) else ""
            abstract = document['_source']['abstract'] if isinstance(document['_source']['abstract'], str) else ""
            text = is_field_in_doc(document['_source'], "text")

            concordance = []
            terms = []
            if term:
                terms = toTermList(term)
            if abstract and term:
                concordance = getConcordance(abstract, terms, 100, 100)
            if text and term:
                concordance = concordance + getConcordance(text, terms, 100, 100)

            date = extract_date(document['_source'])
            if is_field_in_doc(document['_source'], "doctype") != None:
                doctype = extract_doctype(is_field_in_doc(document['_source'], "doctype"))
            else:
                doctype = None
            if document['_source']['longid'] == semantic_sort_id:
                # for semantic sort the semantic_sort_id document is shown on top page and must be removed from the list
                continue
            documents["documents"].append({"id": document['_source']['longid'],
                                           "title": title,
                                           "date": date,
                                           "source": document['_source']['source'],
                                           "score": document['_score'],
                                           "eurovoc": is_field_in_doc(document['_source'], "eurovoc"),
                                           "cites_work": is_field_in_doc(document['_source'], "cites_work"),
                                           "date_start": is_field_in_doc(document['_source'], "date_start"),
                                           "date_end": is_field_in_doc(document['_source'], "date_end"),
                                           "date_year": is_field_in_doc(document['_source'], "date_year"),
                                           "info_force": is_field_in_doc(document['_source'], "info_force"),
                                           "list_lang_original": is_field_in_doc(document['_source'],
                                                                                 "list_lang_original"),
                                           "list_ressource_type": is_field_in_doc(document['_source'],
                                                                                  "list_ressource_type"),
                                           "dts": is_field_in_doc(document['_source'], "dts"),
                                           "service_responsible": is_field_in_doc(document['_source'],
                                                                                  "service_responsible"),
                                           "list_created_by_agent": is_field_in_doc(document['_source'],
                                                                                    "list_created_by_agent"),
                                           "id_sector": is_field_in_doc(document['_source'], "id_sector"),
                                           "concept_directory_1": is_field_in_doc(document['_source'],
                                                                                  "concept_directory_1"),
                                           "concept_directory_2": is_field_in_doc(document['_source'],
                                                                                  "concept_directory_2"),
                                           "concept_directory_3": is_field_in_doc(document['_source'],
                                                                                  "concept_directory_3"),
                                           "subject_matter_1": is_field_in_doc(document['_source'], "subject_matter_1"),
                                           "based_on_legal": is_field_in_doc(document['_source'], "based_on_legal"),
                                           "based_on_concept_treaty": is_field_in_doc(document['_source'],
                                                                                      "based_on_concept_treaty"),
                                           "related_work": is_field_in_doc(document['_source'], "related_work"),
                                           "adopts_resource": is_field_in_doc(document['_source'], "adopts_resource"),
                                           "eurovoc_dom": is_field_in_doc(document['_source'], "eurovoc_dom"),
                                           "eurovoc_mth": is_field_in_doc(document['_source'], "eurovoc_mth"),
                                           "eurovoc_tt": is_field_in_doc(document['_source'], "eurovoc_tt"),
                                           "eurovoc_concept": is_field_in_doc(document['_source'], "eurovoc_concept"),
                                           "doc_type": doctype,
                                           "celex_links": celex_links_to_json(document['_source']),
                                           "concordance": concordance})
            tot = tot + 1
    return jsonify(documents)


def corpus2(term, n_docs, from_doc, sources, sector, subject, res_type, eurovoc_dom, eurovoc_mth, eurovoc_tt,
           eurovoc_concept, conc_dir_1, conc_dir_2, conc_dir_3, info_force, sort, semantic_sort_id, emb_vector, author,
           date_range, aggs):
    documents = {"total_docs": None, "documents": []}
    list_of_aggs_fields = ["date_year", "source", "doc_type", "eurovoc_concept"]
    # if aggs and aggs not in list_of_aggs_fields:
    #     response = jsonify('Malformed query. Wrong aggs parameter')
    #     response.status_code = 404
    #     return response
    
    body = build_corpus_request(term, n_docs, from_doc, sources, sector, subject, res_type, eurovoc_dom, eurovoc_mth,
                                eurovoc_tt, eurovoc_concept, conc_dir_1, conc_dir_2, conc_dir_3, info_force, sort,
                                semantic_sort_id, emb_vector, author, date_range, aggs)
    print(body)
    res = es.msearch(body=body)
    for k in res["responses"]:
        if "error" in k:
            response = jsonify('Malformed query.')
            response.status_code = 404
            return response
        documents["total_docs"] = k['hits']['total']['value']
        if semantic_sort_id is not None and term is None and documents["total_docs"] > 100:
            documents["total_docs"] = 100
        tot = 0
        if aggs:
            documents["aggregations"] = k["aggregations"]["types"]["buckets"]
        for document in k["hits"]["hits"]:
            if tot >= documents["total_docs"]:
                break
            title = document['_source']['title'] if isinstance(document['_source']['title'], str) else ""
            abstract = document['_source']['abstract'] if isinstance(document['_source']['abstract'], str) else ""
            text = is_field_in_doc(document['_source'], "text")

            concordance = []
            terms = []
            if term:
                terms = toTermList(term)
            if abstract and term:
                concordance = getConcordance(abstract, terms, 100, 100)
            if text and term:
                concordance = concordance + getConcordance(text, terms, 100, 100)

            date = extract_date(document['_source'])
            if is_field_in_doc(document['_source'], "doctype") != None:
                doctype = extract_doctype(is_field_in_doc(document['_source'], "doctype"))
            else:
                doctype = None
            if document['_source']['longid'] == semantic_sort_id:
                # for semantic sort the semantic_sort_id document is shown on top page and must be removed from the list
                continue
            documents["documents"].append({"id": document['_source']['longid'],
                                           "title": title,
                                           "date": date,
                                           "source": document['_source']['source'],
                                           "score": document['_score'],
                                           "eurovoc": is_field_in_doc(document['_source'], "eurovoc"),
                                           "cites_work": is_field_in_doc(document['_source'], "cites_work"),
                                           "date_start": is_field_in_doc(document['_source'], "date_start"),
                                           "date_end": is_field_in_doc(document['_source'], "date_end"),
                                           "date_year": is_field_in_doc(document['_source'], "date_year"),
                                           "info_force": is_field_in_doc(document['_source'], "info_force"),
                                           "list_lang_original": is_field_in_doc(document['_source'],
                                                                                 "list_lang_original"),
                                           "list_ressource_type": is_field_in_doc(document['_source'],
                                                                                  "list_ressource_type"),
                                           "dts": is_field_in_doc(document['_source'], "dts"),
                                           "service_responsible": is_field_in_doc(document['_source'],
                                                                                  "service_responsible"),
                                           "list_created_by_agent": is_field_in_doc(document['_source'],
                                                                                    "list_created_by_agent"),
                                           "id_sector": is_field_in_doc(document['_source'], "id_sector"),
                                           "concept_directory_1": is_field_in_doc(document['_source'],
                                                                                  "concept_directory_1"),
                                           "concept_directory_2": is_field_in_doc(document['_source'],
                                                                                  "concept_directory_2"),
                                           "concept_directory_3": is_field_in_doc(document['_source'],
                                                                                  "concept_directory_3"),
                                           "subject_matter_1": is_field_in_doc(document['_source'], "subject_matter_1"),
                                           "based_on_legal": is_field_in_doc(document['_source'], "based_on_legal"),
                                           "based_on_concept_treaty": is_field_in_doc(document['_source'],
                                                                                      "based_on_concept_treaty"),
                                           "related_work": is_field_in_doc(document['_source'], "related_work"),
                                           "adopts_resource": is_field_in_doc(document['_source'], "adopts_resource"),
                                           "eurovoc_dom": is_field_in_doc(document['_source'], "eurovoc_dom"),
                                           "eurovoc_mth": is_field_in_doc(document['_source'], "eurovoc_mth"),
                                           "eurovoc_tt": is_field_in_doc(document['_source'], "eurovoc_tt"),
                                           "eurovoc_concept": is_field_in_doc(document['_source'], "eurovoc_concept"),
                                           "doc_type": doctype,
                                           "celex_links": celex_links_to_json(document['_source']),
                                           "concordance": concordance})
            tot = tot + 1
    return jsonify(documents)


def toTermList(term):
    ts = term.replace('(', '').replace(')', '').replace(' ', '_').replace('_AND_', ' ').replace('_OR_', ' ').split(' ')
    return [t.replace('_', ' ').strip("\"") for t in ts]


def getConcordance(text="", phrases=[], width=150, lines=25):  # lines=0 is unlimited number of concordance lines
    mlen = 0
    for p in phrases:
        if mlen < len(p):
            mlen = len(p)
    tokens = word_tokenize(text)
    concInd = ConcordanceIndex(tokens, (lambda s: s.lower()))
    concs = []
    for phrase in phrases:
        conc = concordance(concInd, phrase, width, lines, mlen)
        for i in conc:
            concs.append(i)
    return concs


def concordance(ci, phrase, width=150, lines=25, width_add=10):
    """
    Rewrite of nltk.text.ConcordanceIndex.print_concordance that returns results
    instead of printing them. And accepts phrases.

    See:
    http://www.nltk.org/api/nltk.html#nltk.text.ConcordanceIndex.print_concordance
    """
    ptokens = word_tokenize(phrase.lower())
    context = width // 4  # approx number of words of context

    results = []
    plen = len(ptokens)
    tlen = len(ci._tokens)
    offsets = ci.offsets(ptokens[0])

    if offsets:
        phrase = []
        if lines != 0:
            lines = min(lines, len(offsets))
        else:
            lines = len(offsets)
        for i in offsets:
            if lines <= 0:
                break
            ii = 0
            for y in range(plen):
                if ci._tokens[i + y].lower() == ptokens[y]:
                    if y + 1 == plen:
                        if i - context < 0:
                            left = (' ' * width +
                                    detokenize(ci._tokens[0:i]))
                        else:
                            left = (' ' * width +
                                    detokenize(ci._tokens[i - context:i]))
                        if i + y + context > tlen:
                            right = detokenize(ci._tokens[i + 1 + y:tlen])
                        else:
                            right = detokenize(ci._tokens[i + 1 + y:i + y + context])
                        left = left[-width:]
                        right = right[:width + +width_add]
                        ph = detokenize(ci._tokens[i:i + y + 1])
                        line = (left, ph, right)
                        #                        line = '%s <em>%s</em> %s' % (left, ph, right)
                        #                        line = line[:((2*width)+width_add)]
                        results.append(line)
                        lines -= 1
                else:
                    break
    return results


def detokenize(words):
    """
    Untokenizing a text undoes the tokenizing operation, restoring
    punctuation and spaces to the places that people expect them to be.
    Ideally, `untokenize(tokenize(text))` should be identical to `text`,
    except for line breaks.
    """
    text = ' '.join(words)
    step1 = text.replace("`` ", '"').replace(" ''", '"').replace('. . .', '...')
    step2 = step1.replace(" ( ", " (").replace(" ) ", ") ")
    step3 = re.sub(r' ([.,:;?!%]+)([ \'"`])', r"\1\2", step2)
    step4 = re.sub(r' ([.,:;?!%]+)$', r"\1", step3)
    step5 = step4.replace(" '", "'").replace(" n't", "n't").replace(
        "can not", "cannot")
    step6 = step5.replace(" ` ", " '")
    return step6.strip()


def docbyid(w):
    try:
        q = es.get(index=config['index'], id=w)
        doc = q['_source']
        doc['timestamp'] = doc['@timestamp']
        del doc['@timestamp']
        return doc
    except:
        response = jsonify('ID not found.')
        response.status_code = 404
        return response


def wikisearch(term, n_docs):
    if n_docs is None:
        n_docs = DEFAULT_DOCS_NUMBER
    term = sanitize_input(term)
    res = es.msearch(body=(build_wiki_request(term, n_docs)))
    documents = {"documents": []}
    for k in res["responses"]:
        for w in k["hits"]["hits"]:
            if 'opening_text' not in w['_source']:
                desc = w['_source']['text']
                desc = desc if len(desc) < 80 else desc[:80] + '...'
            else:
                desc = w['_source']['opening_text']
                desc = desc if len(desc) < 80 else desc[:80] + '...'
            documents["documents"].append({"id": w['_source']['title'], "title": desc})
    return jsonify(documents)


def wikibyid(id):
    try:
        q = es.get(index=config['index-wiki'], doc_type='wikiarticle', id=id)
        return q['_source']
    except:
        response = jsonify('ID not found.')
        response.status_code = 404
        return response


def build_graph(term):
    term = sanitize_input(term)
    if not word_exists(term):
        response = jsonify('Term out of vocabulary.')
        response.status_code = 404
        return response
    size = terms_model.wv.vocab[term].count
    graphjs = {"nodes": [{"id": term, "depth": '0', "size": size, 'graph_size': sqrt(size) // 40 + 3}],
               "links": []}
    maxwords = 7
    nodes = [x for x, y in terms_model.wv.most_similar(term, topn=maxwords)]
    done = copy.deepcopy(nodes)
    done2 = []
    for c, n in enumerate(nodes):
        size = terms_model.wv.vocab[n].count
        graphjs["nodes"].append({"id": n, "depth": '1', "size": size, "graph_size": sqrt(size) // 20 + 3})
        graphjs["links"].append({"source": term, "target": n, "value": .2})
        for m in [x for x, y in terms_model.wv.most_similar(n, topn=5)]:
            if m not in done and m != term and m not in done2:
                size = terms_model.wv.vocab[m].count
                done2.append(m)
                graphjs["nodes"].append({"id": m, "depth": '2', "size": size, "graph_size": sqrt(size) // 20 + 3})
                graphjs["links"].append({"source": n, "target": m, "value": .1})
            elif m in done2:
                graphjs["links"].append({"source": n, "target": m, "value": .1})
    return jsonify(graphjs)


def build_tree(term):
    term = sanitize_input(term)
    if not word_exists(term):
        response = jsonify('Term out of vocabulary.')
        response.status_code = 404
        return response
    graphjs = {"nodes": []}

    nodes = [sanitize_input(x) for x, y in terms_model.wv.most_similar(term, topn=20)]
    nodes2 = []
    done = []
    done2 = []
    for i in range(0, len(nodes)):
        n1 = nodes[i]
        if n1 not in done and n1 != term and n1 not in done2:
            done.append(n1)
            r = [n1]
            for j in range(i + 1, len(nodes)):
                n2 = nodes[j]
                if terms_model.similarity(n1, n2) > 0.7:
                    if n2 not in done and n2 != term and n2 not in done2:
                        done2.append(n2)
                        r.append(n2)
            nodes2.append(r)
    nodes2 = nodes2[:15]
    done = list(itertools.chain.from_iterable(nodes2))
    done2 = []
    for r in nodes2:
        r2 = copy.deepcopy(r)
        for n in r:
            for m in [sanitize_input(x) for x, y in terms_model.wv.most_similar(n, topn=7)]:
                if m not in done and m != term and m not in done2:
                    done2.append(m)
                    r2.append(m)
        graphjs["nodes"].append(r2)
    return jsonify(graphjs)


def doc_to_node(doc_id, depth):
    source = document_is_stored(doc_id)
    document = {}
    document["id"] = doc_id
    document["title"] = ''.join(source['title'])[:1000]
    document["abstract"] = ''.join(source['abstract'])[:2000]
    document["source"] = ''.join(source['source'])
    # document["sentences"] = re.sub('\\n+', '\n', ''.join(source.get('sentences', " "))[:2000]).replace('\n', "<br>")
    # document["text"] = re.sub('\\n+', '\n', ''.join(source.get('text', ' '))[:2000]).replace('\n', "<br>")
    date = extract_date(source)
    document["date"] = date
    document["depth"] = depth
    return document


def extract_date(source):
    if 'date' in source and source['date']:
        if 'T' in source['date'][0]:
            date = source['date'][0].split('T')[0]
        else:
            date = source['date'][0]
    else:
        date = ''
    return date


def document_is_stored(document_id):
    q = es.get(index=config['index'], id=document_id)
    if q['found']:
        return q['_source']
    else:
        return False


def get_clusters(term, n_terms):
    if n_terms is None:
        n_terms = DEFAULT_TERM_NUMBER
    if n_terms > 30:
        n_terms = 30
    term = sanitize_input(term)
    if not word_exists(term):
        response = jsonify('Term out of vocabulary.')
        response.status_code = 404
        return response
    words = [w for w, x in terms_model.wv.most_similar_cosmul(term, topn=n_terms)]
    df = pd.DataFrame(terms_model.wv[words], index=words)
    db = DBSCAN(eps=0.3, min_samples=2, metric='cosine', metric_params=None, algorithm='brute', p=None,
                n_jobs=1).fit(df)
    n_clusters_ = len(set(db.labels_)) - (1 if -1 in db.labels_ else 0)
    print('clusters', n_clusters_, db.labels_)
    cl = {n: [] for n in db.labels_}
    for n, k in enumerate(db.labels_): cl[k].append(words[n])
    clusters = {'n_terms': n_terms, 'clusters': []}
    for k in sorted(cl):
        cluster = str(k) if k > -1 else 'Unclustered'
        clusters['clusters'].append({'cluster': cluster, 'words': cl[k]})
    return jsonify(clusters)


def build_decade_graph(word):
    query = {
        "_source": ["date"],
        "size": 0,
        "aggs": {
            "years": {
                "terms": {"field": "date", "size": 1000000}
            }

        },
        "query": {
            "bool": {
                "must": {"multi_match": {
                    "query": word.replace('_', ' '),
                    "type": "phrase",
                    "fields": ["title^10", "opening-text^3", "text", "sentences"]
                }
                },
                "filter": {"match": {"source": "bookshop OR eurlex OR cordis OR pubsy OR opendataportal"}}
            }
        }
    }
    search_arr = []
    search_arr.append({'index': config['index']})
    search_arr.append(query)
    request = ''

    for each in search_arr:
        request += '%s \n' % json.dumps(each)
    res = es.msearch(body=request)
    a = {}
    for r in res["responses"]:
        for k in r["aggregations"]["years"]["buckets"]:
            year = int(k['key_as_string'].split("-")[0]) // 5 * 5
            if year > 2015: continue
            if year < 1990: year = year // 10 * 10
            if year not in a: a[year] = 0
            a[year] += k['doc_count']
    print('conteggio years....:', a)
    x = sorted([str(w) + "-" + str(w + 9) if w < 1990 else str(w) + "-" + str(w + 4) for w in list(a)])
    print('sorted x....:', x)

    y = [a[w] for w in sorted(list(a))]
    print('sorted y....:', y)

    decade = {'graph': [{'years': a, 'x': x, 'y': y}]}
    return jsonify(decade)


def semantic_distance(w1, w2):
    w1 = sanitize_input(w1)
    w2 = sanitize_input(w2)

    if not word_exists(w1):
        response = jsonify('Term 1 out of vocabulary.')
        response.status_code = 404
        return response

    if not word_exists(w2):
        response = jsonify('Term 2 out of vocabulary.')
        response.status_code = 404
        return response

    dist = {'distance': terms_model.distance(w1, w2)}
    return jsonify(dist)


def most_similar(term, term_list, top_n):
    if top_n is None:
        top_n = 3
    if top_n > len(term_list):
        top_n = len(term_list)
    distances = {}
    term = sanitize_input(term)
    if not word_exists(term):
        response = jsonify('Term out of vocabulary.')
        response.status_code = 404
        return response
    for item in term_list:
        item = sanitize_input(item)
        if word_exists(item):
            distances[item] = terms_model.distance(term, item)
    distances = sorted(distances.items(), key=lambda kv: (kv[1], kv[0]))
    out = {'most-similar': []}
    for i in distances[:top_n]:
        out['most-similar'].append({'term': i[0], 'distance': i[1]})
    return jsonify(out)


def word_exists(path):
    path = sanitize_input(path)
    try:
        terms_model.wv.vocab[path]
        return True
    except:
        return False


def tellnow_rmme():
    return datetime.datetime.now()


def parse_search_term(search_term):
    phrase = []
    if '"' not in search_term:
        best = search_term
        return best, phrase
    if search_term.count('"') % 2 == 0:
        text = search_term
        search_results = re.finditer(r'\".*?\"', text)
        for item in search_results:
            text = text.replace(item.group(0), '')
            phrase.append(item.group(0).replace('"', ''))
        best = text
        return best, phrase
    else:
        best = search_term
        return best, phrase


def bluid_search_phrase_block(search_term_phrase):
    full_block = []
    for text in search_term_phrase:
        block = {"multi_match": {
            "query": text,
            "type": "phrase",
            "fields": ["title^10", "abstract^3", "sentences", "text"],
            "zero_terms_query": "all"
        }}
        full_block.append(block)
    return full_block


def get_sources_param(sources):
    param = ''
    if not sources:
        param = 'source:bookshop OR source:cordis OR source:pubsy OR source:opendataportal'
        return param
    else:
        for source in sources:
            if source == 'eurlex':
                if param == '':
                    param = param + '(source:bookshop AND longid:celex)'
                else:
                    param = param + ' OR (source:bookshop AND longid:celex)'
            if source == 'bookshop':
                if param == '':
                    param = param + '(source:bookshop AND longid:cellar)'
                else:
                    param = param + ' OR (source:bookshop AND longid:cellar)'
            else:
                if param == '':
                    param = param + 'source:' + source
                else:
                    param = param + ' OR source:' + source
        return param


def bluid_metadata_param_blocks(sector, subject, res_type, eurovoc_dom, eurovoc_mth, eurovoc_tt, eurovoc_concept,
                                conc_dir_1, conc_dir_2, conc_dir_3, info_force, author, date_range):
    full_block = []
    if sector:
        or_block = {"bool": {"should": []}}
        for param in sector:
            block = {"match": {"id_sector.keyword": param}}
            or_block['bool']['should'].append(block)
        full_block.append(or_block)
    if subject:
        for param in subject:
            block = {"match": {"subject_matter_1.keyword": param}}
            full_block.append(block)
    if res_type:
        or_block = {"bool": {"should": []}}
        for param in res_type:
            block = {"match": {"list_ressource_type.keyword": param}}
            or_block['bool']['should'].append(block)
        full_block.append(or_block)
    if eurovoc_dom:
        or_block = {"bool": {"should": []}}
        for param in eurovoc_dom:
            block = {"match": {"eurovoc_dom.keyword": param}}
            or_block['bool']['should'].append(block)
        full_block.append(or_block)
    if eurovoc_mth:
        or_block = {"bool": {"should": []}}
        for param in eurovoc_mth:
            block = {"match": {"eurovoc_mth.keyword": param}}
            or_block['bool']['should'].append(block)
        full_block.append(or_block)
    if eurovoc_tt:
        or_block = {"bool": {"should": []}}
        for param in eurovoc_tt:
            block = {"match": {"eurovoc_tt.keyword": param}}
            or_block['bool']['should'].append(block)
        full_block.append(or_block)
    if eurovoc_concept:
        or_block = {"bool": {"should": []}}
        for param in eurovoc_concept:
            block = {"match": {"eurovoc_concept.keyword": param}}
            or_block['bool']['should'].append(block)
        full_block.append(or_block)
    if conc_dir_1:
        or_block = {"bool": {"should": []}}
        for param in conc_dir_1:
            block = {"match": {"concept_directory_1.keyword": param}}
            or_block['bool']['should'].append(block)
        full_block.append(or_block)
    if conc_dir_2:
        or_block = {"bool": {"should": []}}
        for param in conc_dir_2:
            block = {"match": {"concept_directory_2.keyword": param}}
            or_block['bool']['should'].append(block)
        full_block.append(or_block)
    if conc_dir_3:
        or_block = {"bool": {"should": []}}
        for param in conc_dir_3:
            block = {"match": {"concept_directory_3.keyword": param}}
            or_block['bool']['should'].append(block)
        full_block.append(or_block)
    if info_force is not None:
        block = {"match": {"info_force": info_force}}
        full_block.append(block)
    if author:
        or_block = {"bool": {"should": []}}
        for param in author:
            block = {"match": {"list_created_by_agent.keyword": param}}
            or_block['bool']['should'].append(block)
        full_block.append(or_block)
    if date_range:
        range_block = {"range": {"date": {}}}
        for param in date_range:
            var = param.split(':')
            range_block["range"]["date"][var[0]] = var[1]
        full_block.append(range_block)
    return full_block


def build_search_query(search_term, sources, sector, subject, res_type, eurovoc_dom, eurovoc_mth,
                       eurovoc_tt, eurovoc_concept, conc_dir_1, conc_dir_2, conc_dir_3, info_force,
                       author, date_range, aggs):
    query = build_search_query_json(search_term, sources)

    metadata_param_blocks = bluid_metadata_param_blocks(sector, subject, res_type, eurovoc_dom, eurovoc_mth,
                                                        eurovoc_tt, eurovoc_concept, conc_dir_1, conc_dir_2, conc_dir_3,
                                                        info_force, author, date_range)
    if len(metadata_param_blocks) > 0:
        for block in metadata_param_blocks:
            query['bool']['must'].append(block)
    return query


def build_search_query_json(search_term, sources):
    sources_param = get_sources_param(sources)
    #here
    if not search_term:
        query_string = '{"bool": {"must": [{"query_string": {"query": "' + sources_param + '","type": "cross_fields" }}]}}'
        query = json.loads(query_string)
        return query
    search_term = search_term.replace('/', '\\\/')
    if 'AND' in search_term or 'OR' in search_term:
        search_term = search_term.replace('"', '\\"')
        query_string = '{"bool": {"must": [{"query_string": {"fields": ["title^10","abstract^3","text"],"query": "' \
                       + search_term + '","type": "phrase" }},{"query_string": {"query": "' \
                       + sources_param + '","type": "cross_fields" }}]}}'
        query = json.loads(query_string)
    else:
        search_term_best, search_term_phrase = parse_search_term(search_term)
        query_string = '{"bool": {"must": [ {"multi_match": {"query": "' + search_term_best + \
                       '","type": "best_fields","fields": ["title^10", "abstract^3", "sentences", "text"],' \
                       '"zero_terms_query": "all"}},{"query_string" : {"query": "' + sources_param + '","type":  "cross_fields"}}]}}'
        query = json.loads(query_string)
        phrase_block = []
        if len(search_term_phrase) > 0:
            phrase_block = bluid_search_phrase_block(search_term_phrase)
        if len(phrase_block) > 0:
            for block in phrase_block:
                query['bool']['must'].append(block)
    return query


def build_sort_body(sort):
    sort_list = []
    for item in sort:
        item_list = item.split(':')
        sort_item = {item_list[0]: {"order": item_list[1]}}
        sort_list.append(sort_item)
    return sort_list


def build_corpus_request(term, n_docs, from_doc, sources, sector, subject, res_type, eurovoc_dom, eurovoc_mth,
                         eurovoc_tt, eurovoc_concept, conc_dir_1, conc_dir_2, conc_dir_3, info_force, sort,
                         semantic_sort_id, emb_vector, author, date_range, aggs):
    query = build_search_query(term, sources, sector, subject, res_type, eurovoc_dom, eurovoc_mth, eurovoc_tt,
                               eurovoc_concept, conc_dir_1, conc_dir_2, conc_dir_3, info_force, author, date_range,
                               aggs)
    if semantic_sort_id or emb_vector:
        sort = None
        if semantic_sort_id:
            vector = get_vector(semantic_sort_id)
        else:
            vector = emb_vector
        if not vector:
            response = jsonify('Sbert vector is not retrieved')
            response.status_code = 404
            return response
        query_to_use = {"script_score": {
            "query": query, "script": {
                "source": "1 / (1 + l1norm(params.queryVector, doc['sbert_vector']))",
                "params": {"queryVector": vector}}}}
    else:
        query_to_use = query
    if n_docs is None:
        n_docs = DEFAULT_DOCS_NUMBER
    if from_doc is None:
        from_doc = DEFAULT_FROM_DOC_NUMBER
    if sort:
        sort_list = build_sort_body(sort)

    body = {
        "size": n_docs,
        "from": from_doc,
        "track_total_hits": True,
        "query": query_to_use,
        "_source": ["longid", "title", "abstract", "doctype", "source", "eurovoc", "cites_work", "date_start",
                    "date_end", "date_year", "info_force", "list_lang_original", "list_ressource_type", "dts",
                    "service_responsible", "list_created_by_agent", "id_sector", "concept_directory_1",
                    "concept_directory_2", "concept_directory_3", "subject_matter_1", "based_on_concept_treaty",
                    "related_work", "adopts_resource", "eurovoc_dom", "eurovoc_mth", "eurovoc_tt",
                    "eurovoc_concept", "date", "links", "text"]
    }
    if sort:
        body['sort'] = []
        for sort_item in sort_list:
            body['sort'].append(sort_item)
    if aggs:
        if aggs == "source" or aggs == "eurovoc_concept":
            aggs = aggs + '.keyword'
        body['aggs'] = {"types": {"terms": {"field": aggs}}}
    search_arr = []
    search_arr.append({'index': config['index']})
    search_arr.append(body)
    request = ''
    for each in search_arr:
        request += '%s \n' % json.dumps(each)
    return request


def build_wiki_request(path, n_docs):
    body = {
        'size': n_docs,
        "query": {
            "multi_match": {
                "query": path.replace("_", " "),
                "type": "phrase",
                "fields": ["title^10", "opening-text^3", "text"]
            }
        }
    }
    search_arr = []
    search_arr.append({'index': config['index-wiki'], 'type': 'wikiarticle'})
    search_arr.append(body)
    request = ''
    for each in search_arr:
        request += '%s \n' % json.dumps(each)
    return request


if __name__ == '__main__':
    app.run(host=config['api-host'], port=config['api-port'])
