import os
import time
from contextlib import nullcontext
from datetime import datetime

import jwt
import requests
from cas import CASClient
from flask import Blueprint, Flask, Response
from flask import current_app as app
from flask import (json, redirect, request, send_from_directory, session,
                   url_for)
from flask_jwt_extended import get_jwt, jwt_required
from flask_jwt_extended.utils import create_access_token

import config
from auth import authenticateJwt
from db_revoked_tokens_broker import addRevokedToken, getAllUserRevokedTokensDb
from db_users_broker import addDbUser, getDbUser

ecas = Blueprint("ecas", __name__)


@ecas.route("/seta")
def seta(method=["GET"]):
    if app.config['FLASK_ENV'] == "serve":
        root = app.config['ANGULAR_PATH'] + "/"
    else:
        root = app.config['FLASK_PATH'] + "/"

    jawt = nullcontext

    if "username" in session:
        iat = time.time()
        additional_claims = {
            "user": session["user_attributes"],
            "iat": iat,
            "iss": "SETA Flask server",
            "sub": session["username"],
        }
        jawt = create_access_token(
            identity=session["username"], additional_claims=additional_claims)
        # jawt = jwt.encode(additional_claims, app.config['JWT_SECRET_KEY'], algorithm="HS256")

        # return json.jsonify(access_token=jawt)
        """ redir = redirect(root + "seta-ui/#/home")
        redir.headers = {'Authorization': 'Bearer ' + jawt}
        return redir """

        # return redirect(root + "seta-ui/#/home?accessToken=" + jawt)

    return redirect(root + "seta-ui/#/home?accessToken=" + jawt)


@ecas.route("/login")
def login():
    if "username" in session:
        # Already logged in

        attributes = session["user_attributes"]
        if not getDbUser(attributes["uid"]):
            addDbUser(attributes)

        return redirect(url_for("ecas.seta"))

    # next = request.args.get("next")
    ticket = request.args.get("ticket")
    if not ticket:
        # No ticket, the request come from end user, send to CAS login
        cas_login_url = app.cas_client.get_login_url()
        app.logger.debug("CAS login URL: %s", cas_login_url)
        return redirect(cas_login_url)

    # There is a ticket, the request come from CAS as callback.
    # need call `verify_ticket()` to validate ticket and get user profile.
    app.logger.debug("ticket from CAS is: %s", ticket)
    # app.logger.debug("next is: %s", next)

    user, attributes, pgtiou = app.cas_client.verify_ticket(ticket)

    app.logger.debug(
        "CAS verify ticket response: user: %s, attributes: %s, pgtiou: %s",
        user,
        attributes,
        pgtiou,
    )

    if not user:
        return 'Failed to verify ticket. <a href="/login">Login</a>'
    else:  # Login successful, redirect according to `next` query parameter.

        if not getDbUser(attributes["uid"]):
            addDbUser(attributes)
        session["username"] = user
        session["user_attributes"] = attributes
        r5 = redirect(url_for("ecas.seta"))
        return r5


@ecas.route("/logout-ecas")
@jwt_required()
def logoutEcas():
    redirect_url = url_for("ecas.logoutCallback", _external=True)
    cas_logout_url = app.cas_client.get_logout_url(redirect_url)
    app.logger.debug("CAS logout URL: %s", cas_logout_url)

    return redirect(cas_logout_url)


@ecas.route("/logout", methods=["POST"])
@jwt_required()
def logout():

    req = json.loads(request.data.decode("UTF-8"))

    # If request is valid and authorized, also revoke the token for future access, before its
    # normal expiry moment. Revoke by adding it to the MongoDB collection of revoked tokens:
    # if authenticateJwt(req["username"]):
    #     addRevokedToken(req["username"], req["jwt"], str(datetime.now()))
    jti = get_jwt()["jti"]
    # req["jwt"]
    addRevokedToken(req["username"], jti, str(datetime.now()))
    session.pop("username", None)

    response = json.jsonify({"status": "ok"})
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


@ecas.route("/logoutCallback")
def logoutCallback():
    session.pop("username", None)
    return redirect(url_for("ecas.seta"))


@ecas.route("/revoked-tokens/all")
def getAllRevokedTokens():

    tokens = getAllUserRevokedTokensDb()

    response = json.jsonify({"tokens": tokens, "status": "ok"})
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response
