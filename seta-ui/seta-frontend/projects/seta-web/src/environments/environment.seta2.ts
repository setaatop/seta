export const environment = {
  production: false,
  test: false,
  baseUrl: `https://seta2.emm4u.eu/`,
  baseApplicationContext: `/seta/`,
  restEndPoint: `rest/`,
  _regex: new RegExp(`_`, `g`),
};
