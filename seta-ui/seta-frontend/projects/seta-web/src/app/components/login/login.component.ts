import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { faUser, faUserCircle } from '@fortawesome/free-regular-svg-icons';
import { faCopy, faSquare, faUserSlash } from '@fortawesome/free-solid-svg-icons';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from 'projects/seta-web/src/environments/environment';
import { User } from '../../models/user.model';
import { AppToastService } from '../../services/app-toast.service';
import { AuthenticationService } from '../../services/authentication.service';
import { RestService } from '../../services/rest.service';
import { RsaKeysService } from '../../services/rsa-keys.service';
import { NgbdModalConfirm } from '../modals/modals';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  userData: User = null;
  jwtHelper: JwtHelperService;
  displayEcasLogoutLink: boolean = !environment.production && !environment.test
  displaySetaLogoutLink = true;
  displayTestCallsPallete = false;
  http: HttpClient;

  public faUserCircle = faUserCircle;
  public faUser = faUser;
  public faSquare = faSquare;
  public faUserSlash = faUserSlash;
  public faCopy = faCopy;

  environment = environment;

  @Output()
  isRsaKeyBeingGenerated: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private activeRouter: ActivatedRoute,
    private rest: RestService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private _modalService: NgbModal,
    private rsaKeysService: RsaKeysService,
    private toastService: AppToastService,
  ) {
    this.ngOnInit();
  }

  logout() {
    this.authenticationService.setaLogout()
    this.router.navigate(['/home']);
  }

  withAutofocus = `<button type="button" ngbAutofocus class="btn btn-danger"
      (click)="modal.close('Ok click')">Ok</button>`;

  ngOnInit(): void {
    this.authenticationService.loginComponent = this;
    this.jwtHelper = new JwtHelperService();

    // Do the check if token has expired, once when the login component loads. This is needed
    // for the first time when the page is loaded, to do this check and log user out if
    // login token is expired.
    this.activeRouter.queryParams.subscribe((params) => {
      console.log(params)
      
      const validationResult: any = this.authenticationService.readToken(params);

      if (validationResult !== undefined && validationResult.validated) {
        if (validationResult !== undefined && validationResult.validated) {
          try {
            let decodedToken = validationResult.decodedToken;
            this.userData = decodedToken['user'];
            this.userData.username = this.userData.uid ? this.userData.uid : null;

          } catch (e) {

          }
        } else {
          console.log('validation failed');
        }
      }
    });
  }

  goToLink(event: any, link: string) {
    window.location.href = `${environment.baseUrl}${link}`;
    event.preventDefault();
  }

  navigateTo(event: any, link: string) {
    this.router.navigate([`/${link}`]);
  }

  public isUserSet() {
    let is = this.userData !== null && this.userData.username;

    return is;
  }

  open() {
    const modalRef = this._modalService.open(NgbdModalConfirm);
    modalRef.componentInstance.username = this.authenticationService.getCurrentUsername();
    modalRef.result.then(() => {
      this.deleteUser()
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  public deleteUser() {
    this.rest.deleteCurrentUserAccount();
  }

  public generateRsaKeys() {
    this.rsaKeysService.generateRsaKeys().subscribe((r) => {
      this.downLoadFile(r['privateKey'], 'text/plain', `id_rsa`);
      this.toastService.success('RSA key pair successfully generated!');
    });
  }

  /**
   * Method is use to download file.
   * @param data - Array Buffer data
   * @param headers - request Headers
   */
  downLoadFile(data: any, headers: any, filename: string) {
    const link = document.createElement(`a`);
    const blob = new Blob([data], { type: (typeof headers) === 'object' ? headers.get(`Content-Type`) : headers });
    link.href = window.URL.createObjectURL(blob);
    link.download = filename;
    link.click();
    window.URL.revokeObjectURL(link.href);
  }

  // public signMessage() {
  //   this.rest.signMessage("Some_text")
  // }


}
