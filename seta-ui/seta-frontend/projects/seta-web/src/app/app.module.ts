import { CommonModule } from "@angular/common";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations"; // this is needed!
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import {
  NgbCollapseModule,
  NgbDropdownModule,
  NgbModalModule,
  NgbToastModule,
  NgbTooltipModule
} from "@ng-bootstrap/ng-bootstrap";
import { NgSelectModule } from "@ng-select/ng-select";
import { NgxsModule } from "@ngxs/store";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { TagInputModule } from "ngx-chips";
import { DeviceDetectorModule } from "ngx-device-detector";
import { AccordionModule } from "primeng/accordion";
import { ButtonModule } from "primeng/button";
import { MenuModule } from "primeng/menu";
import { TreeModule } from 'primeng/tree';
import {ToastModule} from 'primeng/toast';
import {ContextMenuModule} from 'primeng/contextmenu';
import {DialogModule} from 'primeng/dialog';
import { AppComponent, NgbdModalContent } from "./app.component";
import { routing } from "./app.routes";
import { AboutComponent } from "./components/about/about.component";
import { AppToastsComponent } from "./components/app-toasts/app-toasts.component";
import { CorpusFindallCardComponent } from "./components/corpus-findall-card/corpus-findall-card.component";
import { CorpusListComponent } from "./components/corpus-list/corpus-list.component";
import { CorpusOverviewComponent } from "./components/corpus-overview/corpus-overview.component";
import { CorpusComponent } from "./components/corpus/corpus.component";
import { DecadesGraphComponent } from "./components/decades-graph/decades-graph.component";
import { EurlexFiltersComponent } from "./components/eurlex-filters/eurlex-filters.component";
import { FindAllComponent } from "./components/find-all/find-all.component";
import { FooterComponent } from "./components/footer/footer.component";
import { HomeComponent } from "./components/home/home.component";
import { LoginComponent } from "./components/login/login.component";
import { MainNavbarComponent } from "./components/main-navbar/main-navbar.component";
import { NgbdModalCopy } from "./components/modals/modals";
import { MyAccountComponent } from "./components/my-account/my-account.component";
import { MySearchesComponent } from "./components/my-searches/my-searches.component";
import { NestedDynamicViewComponent } from "./components/nested-dynamic-view/nested-dynamic-view.component";
import { SearchCorpusChipsComponent } from "./components/search-corpus-chips/search-corpus-chips.component";
import { SearchFormComponent } from "./components/search-form/search-form.component";
import { SetaAdvancedFiltersComponent } from "./components/seta-advanced-filters/seta-advanced-filters.component";
import { SimilarGraphComponent } from "./components/similar-graph/similar-graph.component";
import { SimilarComponent } from "./components/similar/similar.component";
import { TickDocumentComponent } from "./components/tick-document/tick-document.component";
import { WikiComponent } from "./components/wiki/wiki.component";
import { ClickOutsideDirective } from "./directives/click-outside.directive";
import { CanActivateUserGuard } from "./guards/can-activate-user.guard";
import { HttpErrorInterceptor } from "./interceptors/http-error.interceptor";
import { HttpJwtInterceptor } from "./interceptors/http-jwt.interceptor";
import { ReplacePipe } from "./pipes/replace.pipe";
import { ngxsConfig } from "./store/ngxs.config";
import { SetaStateCorpus } from "./store/seta-corpus.state";
import { SetaState } from "./store/seta.state";
import { TreeModule as tmodule } from '@circlon/angular-tree-component';
import { DynamicTreeComponent } from "./components/dynamic-tree/dynamic-tree.component";
import { DeleteFolderComponent } from './components/modals/delete-folder/delete-folder.component';
import { QueryStoreTreeComponent } from './components/query-store-tree/query-store-tree.component';


@NgModule({
  declarations: [
    AppComponent,
    SimilarComponent,
    CorpusComponent,
    SimilarGraphComponent,
    MainNavbarComponent,
    SearchFormComponent,
    WikiComponent,
    CorpusListComponent,
    HomeComponent,
    CorpusOverviewComponent,
    DecadesGraphComponent,
    FindAllComponent,
    ReplacePipe,
    FooterComponent,
    ClickOutsideDirective,
    CorpusFindallCardComponent,
    AboutComponent,
    NgbdModalContent,
    NestedDynamicViewComponent,
    SearchCorpusChipsComponent,
    SetaAdvancedFiltersComponent,
    EurlexFiltersComponent,
    AppToastsComponent,
    TickDocumentComponent,
    LoginComponent,
    MyAccountComponent,
    MySearchesComponent,
    NgbdModalCopy,
    DynamicTreeComponent,
    DeleteFolderComponent,
    QueryStoreTreeComponent,
  ],
  imports: [
    CommonModule,
    TagInputModule,
    BrowserAnimationsModule,
    BrowserModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    routing,
    FormsModule,
    AccordionModule,
    ButtonModule,
    MenuModule,
    NgbDropdownModule,
    NgSelectModule,
    NgbCollapseModule,
    HttpClientModule,
    DeviceDetectorModule,
    NgbModalModule,
    NgbTooltipModule,
    NgbDropdownModule,
    NgbToastModule,
    NgxsModule.forRoot([SetaState, SetaStateCorpus], ngxsConfig),
    NgxDatatableModule.forRoot({
      messages: {
        emptyMessage: `No data to display`, // Message to show when array is presented, but contains no values
        totalMessage: `total`, // Footer total message
        selectedMessage: `selected` // Footer selected message
      }
    }),
    TreeModule,
    ToastModule,
    ContextMenuModule,
    DialogModule,
    tmodule

  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpJwtInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    }, CanActivateUserGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
