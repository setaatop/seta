export const loginTokenExpiryInterval = 60; // In minutes

export const defaultNoPublicKeyMessage = 'NO PUBLIC KEY IS SET';
