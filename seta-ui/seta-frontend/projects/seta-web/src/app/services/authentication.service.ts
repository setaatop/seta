import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { environment } from '../../environments/environment';
import { LoginComponent } from '../components/login/login.component';
import { User } from '../models/user.model';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  public currentUserSubject: BehaviorSubject<User> = null
  private jwtHelper: JwtHelperService;
  public loginComponent: LoginComponent;
  public baseUrl;
  isTokenDecoded: boolean;
  decodedToken: any;
  accessToken: any;

  constructor(public http: HttpClient) {
    this.jwtHelper = new JwtHelperService();
    this.loginComponent = null;

    let jwt = this.getLocalStorageToken();

    try {
      this.currentUserSubject = new BehaviorSubject<User>(this.jwtHelper.decodeToken(jwt));
    } catch (e) {
      this.setaLogout();
    }

    this.baseUrl = `${environment.baseUrl}${environment.baseApplicationContext}`;
    // this.updateRevokedTokens();
  }

  public get currentUserValue(): User {
    if (null == this.currentUserSubject) {
      return null;
    }
    return this.currentUserSubject.value;
  }

  /* validateAndUpdateRevokedTokens() {
    let rts: any = localStorage.getItem("setaRevokedTokens");
    if (rts === undefined && rts == null) {
      this.updateRevokedTokens().subscribe(() => {
        this.validateToken()
      })
    } else {
      this.validateToken()
    }
  } */

  // Takes JWT token from localStorage, tries to decode it, and (optionally) checks if still
  // not expired. If successful, then returns true in the "validated" response field
  // params - queryString params
  /* validateToken(params = null) {

    this.fetchAndDecodeCurrentToken(params);

    // Do the check if the stored token has been decoded, and not expired
    if (this.isTokenDecoded && this.decodedToken != null) {

      // Check that the token is in course of validity
      const nowTime: number = new Date().getTime();
      const now: number = Math.floor(nowTime / 1000); // now

      const iat: number = this.decodedToken.iat; // time when the token was issued

      let minutesPassedSinceLogin = (now - iat) / 60;

      if (minutesPassedSinceLogin <= environment.loginTokenExpiryInterval) {

        let isRevoked;

        let rts: any = localStorage.getItem("setaRevokedTokens");
        rts = rts.split(",");

        let found = rts.find((rt) => {
          return rt == this.accessToken;
        });

        if (undefined == found) {
          isRevoked = false;
        } else {
          isRevoked = true;
        }

        if (isRevoked) {
          let res = { validated: false, message: "Token is revoked" };
          console.log(res);
          this.setaLogout();
          return res;

        } else {

          localStorage.setItem('accessToken', this.accessToken);
          let u = new User({ ...this.decodedToken['user'] });
          u.username = u.uid;
          this.currentUserSubject.next(u);

          let res = { validated: true, token: this.accessToken, decodedToken: this.decodedToken }
          // console.log(res);
          return res;

        }

      } else {
        // Token has expired
        this.setaLogout();
        let res = { validated: false, message: "Token has expired" };
        console.log(res);
        return res;

      }

    } else {
      // Token could not be properly decoded
      this.setaLogout();
      let res = {
        validated: false, message: "Token couldn't be decoded or is null"
      };
      // console.log(res);
      return res;
    }
  } */

  readToken(params = null) {
    this.fetchAndDecodeCurrentToken(params);
    // Do the check if the stored token has been decoded, and not expired
    if (this.isTokenDecoded && this.decodedToken != null) {
      localStorage.setItem('accessToken', this.accessToken);
      let u = new User({ ...this.decodedToken['user'] })
      u.username = u.uid;
      this.currentUserSubject.next(u);
      let res = {
        validated: true, token: this.accessToken,
        decodedToken: this.decodedToken
      }
      return res;
    }

  }

  public getLocalStorageToken(): string {
    let accessToken = localStorage.getItem('accessToken');
    return accessToken;
  }

  private updateRevokedTokens(): Observable<null> {
    let subject = new Subject<null>()
    //let url = this.baseUrl + 'revoked-tokens/all/' + un;
    let url = this.baseUrl + 'revoked-tokens/all';

    this.http.get(url).subscribe((r: any) => {
      localStorage.setItem("setaRevokedTokens", r.tokens);
      subject.next()
    })
    return subject.asObservable()
  }

  // Get the token and decode it, store the decoded result in a property
  private fetchAndDecodeCurrentToken(params) {
    // The accessToken from the URL has the precedence in the following || operation
    this.accessToken = ((params != null)
      && params['accessToken'])
      || this.getLocalStorageToken();

    try {
      this.decodedToken = this.jwtHelper.decodeToken(this.accessToken);
      this.isTokenDecoded = true;
    } catch (e) {
      this.isTokenDecoded = false;
    }
  }

  setaLogout() {

    // Revoke the token on backend, by inserting it into list of revoked tokens in MongoDB
    if (this.currentUserValue != null) {

      let un = this.getCurrentUsername();

      let url = environment.baseUrl + '/seta-ui/logout';
      let body = {
        username: un,
        jwt: this.getLocalStorageToken()
      };

      this.http.post<any>(url, body).subscribe((r) => {
        console.log(r);
        // this.updateRevokedTokens();
      });
    }

    // Logout on frontend
    if (null != this.loginComponent) {
      this.loginComponent.userData = null;
      if (null == this.currentUserSubject) {

      } else {
        this.currentUserSubject.next(null);
      }
    }

    localStorage.removeItem('accessToken');

  }



  getCurrentUsername(): string {
    let ret = this.currentUserValue ? this.currentUserValue.username ? this.currentUserValue.username : null : null;

    if (undefined == ret || null == ret) {
      ret = null;

    }

    return ret;
  }

  /* public isUserLoggedIn() {
    //just check is user is present
    let validationResponse: any = this.validateToken();
    return validationResponse.validated;
  } */

}
